using IoTHubTrigger = Microsoft.Azure.WebJobs.EventHubTriggerAttribute;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.EventHubs;
using System.Text;
using System.Net;
using System;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Company.Function
{
    public static class IotHubTriggerCSharp1
    {
        /// this function takes a message from the iot hub, converts it into influx-compliant data, and sends it to the influxdb instance.
        [FunctionName("IotHubTriggerCSharp1")]
        public static void Run([IoTHubTrigger("messages/events", Connection = "IotHubConnection", ConsumerGroup = "$default")]EventData message, ILogger log)
        {
            var jsonString = Encoding.UTF8.GetString(message.Body.Array);
            var msg = new sensorMessage(jsonString);
            log.LogInformation($"C# IoT Hub trigger function processed a message: {Encoding.UTF8.GetString(message.Body.Array)}");
            var uri = "http://137.135.91.127:8086/write?db=sensor";
            var client = new WebClient();
            var machineData = msg.getMachineInfluxLine();
            var ambientData = msg.getAmbientInfluxLine();
            client.UploadString(uri, machineData);
            client.UploadString(uri, ambientData);
        }

    }


    /// <summary>
    /// this class represents a message sent by the iot edge device sensor to the azure iothub hub.
    /// this class is able to parse the json and convert it to influx-compliant line protocol data.
    /// </summary>
    public class sensorMessage
    {
        public float _machineTemp{get;set;}
        public float _machinePressure{get;set;}
        public float _ambientTemp{get;set;}
        public float _ambientHumidity{get;set;}
        public Boolean _oldMessage{get;set;}
        public long _timeCreated {get;set;}


        /// constructor here
        public sensorMessage(string json)
        {
            _oldMessage = false;
            JObject obj = JObject.Parse(json);
            JToken machine = obj["machine"];
            JToken ambient = obj["ambient"];
            JToken timeCreated = obj["timeCreated"];
            _machineTemp = (float)machine["temperature"];
            _machinePressure = (float)machine["pressure"];
            _ambientTemp = (float)ambient["temperature"];
            _ambientHumidity = (float)ambient["humidity"];
            var parsedDate = DateTime.ParseExact((string)timeCreated, "yyyy-MM-dd-hh:mm:ss", null);
            var currentTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            _timeCreated = (long)(parsedDate - currentTime).TotalSeconds;
        }

        // gets the current message machine's data and converts it to influx-compliant line protocol data.
        public string getMachineInfluxLine()
        {
            return string.Concat("machine,name=brook temperature=", 
            _machineTemp,
            ",pressure=",
            _machinePressure,
            " ",
            _timeCreated
            );
        }

        // gets the current message ambient's data and converts it to influx-compliant line protocol data.
        public string getAmbientInfluxLine()
        {
            return string.Concat("ambient,name=brook temperature=",
            _ambientTemp,
            ",humidity=",
            _ambientHumidity,
            " ",
            _timeCreated
            );
        }

    }
}